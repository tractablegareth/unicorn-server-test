provider "aws" {
  version    = "1.17.0"
  region     = "eu-west-2"
  access_key = ""
  secret_key = ""
}

# VPC
resource "aws_vpc" "unicorn-VPC" {
    cidr_block = "10.99.0.0/16"
}

# Subnet
resource "aws_subnet" "unicorn-Subneta" {
    vpc_id = "${aws_vpc.unicorn-VPC.id}"
    cidr_block = "10.99.2.0/24"
    map_public_ip_on_launch = true
    availability_zone = "eu-west-2a"
}

resource "aws_subnet" "unicorn-Subnetb" {
    vpc_id = "${aws_vpc.unicorn-VPC.id}"
    cidr_block = "10.99.1.0/24"
    map_public_ip_on_launch = true
    availability_zone = "eu-west-2b"
}
# IGW
resource "aws_internet_gateway" "unicorn-IGW" {
    vpc_id = "${aws_vpc.unicorn-VPC.id}"
}

# Route Table
resource "aws_route" "unicorn-Route" {
    route_table_id = "${aws_vpc.unicorn-VPC.main_route_table_id}"
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.unicorn-IGW.id}"
}

# Route table association
resource "aws_route_table_association" "route_assoc_a" {
    subnet_id = "${aws_subnet.unicorn-Subneta.id}"
    route_table_id = "${aws_route.unicorn-Route.route_table_id}"
}
resource "aws_route_table_association" "route_assoc_b" {
    subnet_id = "${aws_subnet.unicorn-Subnetb.id}"
    route_table_id = "${aws_route.unicorn-Route.route_table_id}"
}

# Security group - Server
resource "aws_security_group" "unicorn-SecGroup" {
    name = "main_security_group"
    vpc_id = "${aws_vpc.unicorn-VPC.id}"

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1" # all protocols
        cidr_blocks = ["0.0.0.0/0"]
    }
}

# Security Group - LB
resource "aws_security_group" "unicorn-SecGroupLB" {
    name = "lb_security_group"
    vpc_id = "${aws_vpc.unicorn-VPC.id}"

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1" # all protocols
        cidr_blocks = ["0.0.0.0/0"]
    }
}

# Application Load Balancer
resource "aws_lb" "unicorn-lb" {
  name            = "unicorn-load-balancer"
  internal        = false
  subnets         = ["${aws_subnet.unicorn-Subneta.id}","${aws_subnet.unicorn-Subnetb.id}"]
  security_groups = ["${aws_security_group.unicorn-SecGroupLB.id}"]
  enable_deletion_protection = false
}

# ALB - Target Group
resource "aws_lb_target_group" "unicorn-tg" {
  name     = "unicorn-target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${aws_vpc.unicorn-VPC.id}"

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    interval            = 5
    path                = "/index.html"
    port                = "80"
  }
}

# ALB Listener - HTTP
resource "aws_lb_listener" "unicorn-listener" {
  load_balancer_arn = "${aws_lb.unicorn-lb.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.unicorn-tg.arn}"
    type             = "forward"
  }
}

# keypair
resource "aws_key_pair" "unicorn-key" {
  key_name   = "unicorn-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDR2RpvURLZUTTvM2c3/Lc3yxRjTjG8v/FUHEg+HtLUBFYIYGgBxm8yhnZ2xOhKmCl/SCLUGwSqNehZaML/ZUk0B+RQLpBfDchlGhkCIuD17mv2EnwQ/TQiuhmXaoh53xbQBes7KSFlutO2IWSIdBnZE1xPeski+IFduwJm9ueZXtwUWf+nLta/giAJ3+bKY/qUi2/qqSgKtBzemwkO3ExTALyBjZLD+N464erukAy3x/+UqJgDsuhyHGFvGL3sMKwA5ptOE/w4GquNHoQzDkOe0BSiQRHx1kU/dxaWmjHIvhwLj8ppTQPzFstjuWqFnTWGUwXcWnFgonvSzdph65L1"
}

# EC2 Instance
resource "aws_instance" "unicorn-server" {
    ami = "ami-00846a67"
    instance_type = "t2.micro"

    key_name = "unicorn-key"

    subnet_id = "${aws_subnet.unicorn-Subneta.id}"
    vpc_security_group_ids = ["${aws_security_group.unicorn-SecGroup.id}"]

    user_data = <<-EOF
                #!/bin/bash
                yum --nogpgcheck -t -y install epel-release
                yum --nogpgcheck -t -y install python-pip git ansible
                pip install docker
                /bin/ansible-pull -d /tmp/ansible \
                  -U https://tractablegareth@bitbucket.org/tractablegareth/unicorn-server-test.git \
                  --accept-host-key --extra-vars "target=localhost" --purge ansible.yml
                EOF
}

# TG attatchment
resource "aws_lb_target_group_attachment" "unicorn-tg-attatchment" {
  target_group_arn = "${aws_lb_target_group.unicorn-tg.arn}"
  target_id        = "${aws_instance.unicorn-server.id}"
}

output "dns_name" {
  description = "The DNS name of the load balancer."
  value       = "${aws_lb.unicorn-lb.dns_name}"
}
